import React from 'react';

import RotatingText from './RotatingText';

class Container extends React.PureComponent {
    constructor(props) {
        super(props);
        this.handleRotate = this.handleRotate.bind(this);
        this.state = {
            rotate: false
        }
    }

    handleRotate() {
        this.setState((prevState) => ({rotate: !prevState.rotate}));
    }

    render() {
        return (
            <div id="container">
                <RotatingText text="RSC RULES" rotate={this.state.rotate} />
                <button onClick={this.handleRotate}>ROTATE</button>
            </div>
        );
    }
}

export default Container;
