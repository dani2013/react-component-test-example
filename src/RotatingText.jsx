import React from 'react';
import PropTypes from 'prop-types';

class RotatingText extends React.PureComponent {
    constructor(props) {
        super(props);
    }

    render() {
        const classes = this.props.rotate ? 'rotating-text rotate' : 'rotating-text';
        return (
            <div className={classes}>{this.props.text}</div>
        );
    }
}

RotatingText.propTypes = {
    text: PropTypes.string,
    rotate: PropTypes.bool,
};

export default RotatingText;
